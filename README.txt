#Introduction
This module adds a Digital Author Identifier (DAI) field to the contributor 
data within the Bibliography module. The Digital Author Identifier is a unique 
national number for every author active within a Dutch university, university 
of applied sciences, or research institute.

#Requirements
This module requires the following module:
Bibliography(https://drupal.org/project/biblio)

#Installation
Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 
for further information.

#Maintainers
Current maintainer:
Gerben Kaas (gkaas) - https://drupal.org/user/459940
