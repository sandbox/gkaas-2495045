<?php
/**
 * @file
 * Installation and update scripts for biblio_dai module.
 */

/**
 * Implements hook_schema_alter().
 */
function biblio_dai_schema_alter(&$schema) {
  $schema['biblio_contributor_data']['fields']['dai'] = array(
    'type' => 'varchar',
    'length' => BIBLIO_DAI_MAX_CHARS,
    'description' => 'Digital Author Identifier (DAI)',
    'default' => '',
  );
  return $schema;
}

/**
 * Implements hook_install().
 */
function biblio_dai_install() {
  // Ensure translations don't break during installation.
  $t = get_t();
  if (db_table_exists('biblio_contributor_data')) {
    $field = array(
      'type' => 'varchar',
      'length' => BIBLIO_DAI_MAX_CHARS,
      'description' => 'Digital Author Identifier (DAI)',
      'default' => '',
    );
    if (!db_field_exists('biblio_contributor_data', 'dai')) {
      // Add dai field to biblio_contributor_data table.
      db_add_field('biblio_contributor_data', 'dai', $field);
    }
  }
  else {
    $message = 'The required biblio_contributor_data table does
    not exist. The Bibliography module is required.';
    drupal_set_message($t($message), 'error');
  }
}

/**
 * Implements hook_uninstall().
 */
function biblio_dai_uninstall() {
  // Drop dai field.
  if (db_field_exists('biblio_contributor_data', 'dai')) {
    db_drop_field('biblio_contributor_data', 'dai');
  }
}
